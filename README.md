# Gamenastix tracker driver for SteamVR #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Simulate SteamVr trackers

### How to test? ###

To test, you must copy the entire contents of the **_bin_** directory (the **_drivers_** directory) to **_C:\Program Files (x86)\Steam\steamapps\common\SteamVR_**.
It also contains all required configuration files for SteamVR.